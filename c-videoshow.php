<?php
class videoshow extends Control {
  private $appId = '你的appid';
  private $appSecret = '你的appsecret';
  public function test_GET(){
      $signPackage = $this->GetSignPackage();
      $data = $signPackage;
      $data['data'] = $this->getmsg();
      // $data['sharecontent']=$this->getshareTimeLine();
      // $data['sharecontent2']=$this->getshareTimeLine2();
      // var_dump($data);exit;     
      $this->display('videoshow',$data);
  }
      //载入一些前台信息     
  public function getmsg(){   
      $arr =array(
        "ad"      =>'1',//后台给一个标准，判断是否有广告的出现
        "readall" =>'0',//阅读全文是否可以点
        "title"   =>"监控实拍KTV老总在包房被欺负,喊来黑帮大姐拿酒瓶爆头...",//文章页分享标题
        "nickname"=>"男性健康咨询",//关注公总号名称
        "readnum" =>'10000+',
        "video"   =>'g0541wjarpq',
        "stoptime"=>'30', //视频暂停时长       
      );
      return $arr;
  }
  // 把分享朋友圈的内容载入到前台
  // public function getshareTimeLine(){
  //      $sharecontent = array(
  //          "sharetitle" => "当前112482在看【一小伙把服务员打进医院，大姐带人狂砍小伙】...",
  //          "sharelink"  => "http://tt.t.vipds.cn/check",
  //          "shareimgUrl"=>"http://shp.qpic.cn/qqvideo_ori/0/g0541wjarpq_496_280/0",
  //      );
  //      return $sharecontent;
  // }
  // 朋友圈的二次分享内容
  // public function getshareTimeLine2(){
  //      $sharecontent2 = array(
  //          "sharetitle2" => "观看视频之后分享内容",
  //          "sharelink2"  => "http://tt.t.vipds.cn/check",
  //          "shareimgUrl2"=>"http://shp.qpic.cn/qqvideo_ori/0/g0541wjarpq_496_280/0",
  //      );
  //      return $sharecontent2;
  // }
  //获得签名包
  public function GetSignPackage() {
      $jsapiTicket = $this->getJsApiTicket();
      $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";//server_port,服务器端口443，网页浏览端口，能提供加密和通过安全端口传输的另一种HTTP。
      $url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";//请求头信息中的Host内容，获取当前域名。当前脚本路径，根目录之后的目录。
      $timestamp = time();
      $nonceStr = $this->createNonceStr();
      // 这里参数的顺序要按照 key 值 ASCII 码升序排序
      $string = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";
      $signature = sha1($string);
      $signPackage = array(
        "appId"     => $this->appId,
        "nonceStr"  => $nonceStr,
        "timestamp" => $timestamp,
        "url"       => $url,
        "signature" => $signature,
        "rawString" => $string
      );
      return $signPackage; 
  }  
  //获取随机字符串
  public function createNonceStr($length = 16) {
      $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      $str = "";
      for ($i = 0; $i < $length; $i++) {
        $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
      }
      return $str;
  }
  //获取jsapiticket
  public function getJsApiTicket() {
      $data = $this->red('cache')->get('appId_jsapi_ticket');
      if ($data !== false) {
          $data = json_decode($data,true);
          if ($data['expire_time'] < time()) {        
                $accessToken = $this->getAccessToken();
                $url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=$accessToken";
                $res = json_decode($this->httpGet($url));
                $ticket = $res->ticket;
                if ($ticket) {
                  $expire_time = 7000;
                  $data['expire_time'] = time() + $expire_time;
                  $expire_time = $data['expire_time'];
                  $data['jsapi_ticket'] = $ticket;
                  $this->red('cache')->setex('appId_jsapi_ticket',$expire_time,json_encode($data,true));
                }
          }else{
              $data['expire_time'] = $data['expire_time'];
              $data['jsapi_ticket'] = $data['jsapi_ticket'];
          }
      }else{
          $accessToken = $this->getAccessToken();
          $url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=$accessToken";
          $res = json_decode($this->httpGet($url));
          $ticket = $res->ticket;
          if ($ticket) {
            $expire_time = 7000;
            $data['expire_time'] = time() + $expire_time;
            $data['jsapi_ticket'] = $ticket;
            $this->red('cache')->setex('appId_jsapi_ticket',$expire_time,json_encode($data,true));
          }               
      }
      return $data['jsapi_ticket'];
 }
  //获取access_token
  public function getAccessToken(){
      $data = $this->red('cache')->get('appId_access_token');
      if ($data !== false){
          $data = json_decode($data,true);
          if ($data['expire_time'] < time()){
              $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$this->appId}&secret={$this->appSecret}";
              $res = json_decode($this->httpGet($url));
              $access_token = $res->access_token;  //
              if ($access_token) {
                $expire_time = 7000;
                $data['expire_time'] = time() + $expire_time;
                $data['access_token'] = $access_token;   //  
                $this->red('cache')->setex('appId_access_token',$expire_time,json_encode($data,true));
              }
          }else{
               $data['expire_time'] = $data['expire_time'];
               $data['access_token'] = $data['access_token'];
          }
      }else{
          $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$this->appId}&secret={$this->appSecret}";
          $res = json_decode($this->httpGet($url));
          $access_token = $res->access_token;  //
          if ($access_token) {
            $expire_time = 7000;
            $data['expire_time'] = time() + $expire_time;
            $data['access_token'] = $access_token;   //   
            $this->red('cache')->setex('appId_access_token',$expire_time,json_encode($data,true));
          }
      }
      return $data['access_token'];
    }
  //curl函数   https的接口调用
    public function httpGet($url) {
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  //curlopt_returntransfer属性将curl_exec()获取的信息以文件流的形式返回，而不是直接输出。
      curl_setopt($curl, CURLOPT_TIMEOUT, 500);   //curlopt_timeout,500，设置cURL允许执行的最长秒数
      curl_setopt($curl, CURLOPT_URL, $url);    //curlopt_url，，需要获取的URL地址，也可以在curl_init()函数中设置
      $res = curl_exec($curl);//执行给定的cURL会话。
      curl_close($curl);//关闭curl，释放资源
      return $res;
    }  
}

